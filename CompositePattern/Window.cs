using System;

namespace CompositePattern
{
	public abstract class Window
	{
		String nome{ get; set; }

		public Window(String nome)
		{
			this.nome = nome;
		}

		public abstract void desenhar ();
	}
}

