using System;

namespace CompositePattern
{
//	utilizado para representar um objeto com uma composição objetos similares, como se fosse uma árvore.
//	Vamos pensar em uma janela de um aplicativo, podemos dizer que a janela é nossa super classe e dentro dela, hierarquicamente
//	contém vários elementos gráficos, como buttons, labels e inputs.
//	Importante saber que o objeto composto tem de ser capaz de implementar os métodos de sua lista de objetos de maneira repetitiva.
//	Uma vantagem do uso é tratar de maneira uniforme objetos individuais e objetos compostos.
	// Problema: devemos tratar onjetos primitivos e recipientes de modo diferente, mesmo se o usuário trate de maneira idêntica.
	// Maneira recursiva


	//	TODO: Verificar a meneira de criar o método



	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");
		}
	}
}
